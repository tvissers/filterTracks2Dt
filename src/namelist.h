typedef enum {N_I, N_R, N_C, N_LLI} VType;

#define NameI(x)  {#x, &x, N_I, sizeof (x) / sizeof (int)}
#define NameR(x)  {#x, &x, N_R, sizeof (x) / sizeof (float)}
#define NameC(x)  {#x, &x, N_C, sizeof (x) / sizeof (char)}
#define NameLLI(x)  {#x, &x, N_LLI, sizeof (x) / sizeof (long long int)}

typedef struct {
  char *vName;
  void *vPtr;
  VType vType;
  int vLen, vStatus;
} NameList;

#define ValI(x)  {&x, N_I, sizeof (x) / sizeof (int)}
#define ValR(x)  {&x, N_R, sizeof (x) / sizeof (float)}
#define ValC(x)  {&x, N_C, sizeof (x) / sizeof (char)}
#define ValLLI(x)  {&x, N_I, sizeof (x) / sizeof (long long int)}

typedef struct {
  void *vPtr;
  VType vType;
  int vLen;
} ValList;


typedef struct {
  unsigned int width;
  unsigned int height;
  unsigned int bpp;
  unsigned char *bitmap;
} tImage_raw;

typedef struct {
  unsigned int width;
  unsigned int height;
  double *r;
  double *g;
  double *b;
} tImage_rgb;

typedef struct {
  unsigned int width;
  unsigned int height;
  double *bitmap;
} tImage;

typedef struct {
  unsigned int width;
  unsigned int height;
  unsigned int depth;
  double *bitmap;
} tImage3d;

typedef struct {
  double x;
  double y;
  double z;
} dir;

typedef struct {
  double x;
  double y;
} cap;

typedef enum {spin1, spin2, swim, stick, diff, move, ambi, polestick1, polestick2, comstick, comstick_spin1, comstick_spin2, undefined} bacType;
typedef enum {pol1, COM, pol2} rotpoint;

typedef struct {
  double x;
  double y;
  int n;
  dir orientation;
  dir normalized_orientation;
  dir swimdir;
  dir swimbodydir;
  double length;
  double width;
  double accuracy;
  double ar1;
  double ar2;
  double ar3;
  cap pol1;
  cap pol2;
  bacType type;
  rotpoint axis;
  int x_cell;
  int y_cell;
  int neighbours;
  int state;
  int jump;
  double swimmingDirection_cor;
  double bodyOrientation_cor;
  double swimmingSpeed;
  double swimmingInclination;
} tVector;


