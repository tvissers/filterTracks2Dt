------------------------------------------------------------------------------
About: License

    This file is part of filterTracks2Dt.

    filterTracks2Dt is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    filterTracks2Dt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with filterTracks2Dt.  If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------
About: This program

When particles or bacteria are close together, it is possible that the tracking algorithm linked the wrong ones into a trajectory. This code looks where trajectories overlap, removes overlapping segments and exports the non-overlapping segmentsas new trajectories.

Please cite the following papers if you use this code for a scientific publication:

T. Vissers et al., Science Advances, 4, 4, eaao1170, 2018.
https://doi.org/10.1126/sciadv.aao1170

T. Vissers et al., PloS One., 14, 6, e0217823, 2019.
https://doi.org/10.1371/journal.pone.0217823

--------------------------------------------------------------------------------

About: repository

At the time of publication, a repository for this code is available at: https://git.ecdf.ed.ac.uk/tvissers/filterTracks2Dt
Updated versions may appear there.

--------------------------------------------------------------------------------

About: Running the example

Type 'make' to compile the code and create the executable.

To run the testcase a filterTracks2Dt.in should be provided with this program. This file
contains the setting used by the tracking algorithm and should look as follows:

--- text -----------------------------------------------------------------------
TRACKINPUTFILE ../testcase/input/tracks.dat
OUTPUTDIR ../testcase/output/
DEBUG 1
TT 5.0
--------------------------------------------------------------------------------

About: Input files

* tracks.dat file containing trajectories, obtained with trackRods2Dt.

--------------------------------------------------------------------------------

About: Output files

* tracks_fixed.dat file containing filtered trajectories.
* tracks_fixed.chk file indicating which parts of trajectories that were kept and discarted. Typically not needed for further analysis.
* details.info file with information about the output of the algorithm.

--------------------------------------------------------------------------------

About: tracks_fixed.dat - file format

This file contains filtered trajectories. The format follows the format of the input file tracks.dat

--- text -----------------------------------------------------------------------
&NUMBEROFTRAJECTORIES &NUMBEROFFRAMES VERSION_ID
&TRAJECTORYNO &TRAJECTORYDURATION
FRAMENO PARTICLEID X_COM Y_COM X_OR Y_OR LENGTH FITPAR1 FITPAR2 FITPAR3 WIDTH FITPAR4
FRAMENO PARTICLEID X_COM Y_COM X_OR Y_OR LENGTH FITPAR1 FITPAR2 FITPAR3 WIDTH FITPAR4
FRAMENO PARTICLEID X_COM Y_COM X_OR Y_OR LENGTH FITPAR1 FITPAR2 FITPAR3 WIDTH FITPAR4
..
..
..
&TRAJECTORYNO &TRAJECTORYDURATION
FRAMENO PARTICLEID X_COM Y_COM X_OR Y_OR LENGTH FITPAR1 FITPAR2 FITPAR3 WIDTH FITPAR4
..
..
--------------------------------------------------------------------------------

Legend:

--- text -----------------------------------------------------------------------
NUMBEROFTRAJECTORIES    :       total number of trajectories
NUMBEROFFRAMES          :       total number of frames
VERSION_ID              :       negative number is new (default) format, positive number is old format
--------------------------------------------------------------------------------

For each trajectory is written:

--- text -----------------------------------------------------------------------
TRAJECTORYNO           :       ID of the trajectory, starts at 0
TRAJECTORYDURATION     :       Duration (in frames) of the trajectory
--------------------------------------------------------------------------------

For each frame in the trajectory is given:

--- text -----------------------------------------------------------------------
FRAMENO         :       frameno since start of the video
PARTICLEID      :       original ID number of rod-shaped object in the frame
X_COM           :       x position in pixels
Y_COM           :       y position in pixels
X_OR            :       x-component of vector (not normalised) pointing from COM to POLE
Y_OR            :       y-component of vector (not normalised) pointing from COM to POLE
LENGTH          :       length (in pixels)
FITPAR1         :       aspect ratio lengthfromfit /widthfromfit obtained with findRods2Dt
FITPAR2         :       (lengthfromfit-widhtfromfit)/widthfromfit obtained with findRods2Dt
FITPAR3         :       aspect ratio lengthfromfit / estimated width from input file obtained with findRods2Dt
WIDTH           :       width (in pixels)
FITPAR4         :       sqrt(largesteigenvalue/smallesteigenvalue) obtained with findRods2Dt
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

About: tracks_fixed.chk - file format

This file can be used to check if filtering went OK by looking at segments of original trajectories. The format is slightly different.

--- text -----------------------------------------------------------------------
&NUMBEROFSEGMENTS &NUMBEROFFRAMES 1.000000
SEGMENTNO SEGMENTDURATION COLOUR1 COLOUR2
FRAMENO PARTICLEID X_COM Y_COM X_OR_NORMALISED Y_OR_NORMALISED LENGTH 0.5 0.5 0
..
..
SEGMENTNO SEGMENTDURATION COLOUR1 COLOUR2
FRAMENO PARTICLEID X_COM Y_COM X_OR_NORMALISED Y_OR_NORMALISED LENGTH 0.5 0.5 0
..
..
--------------------------------------------------------------------------------

Legend:

--- text -----------------------------------------------------------------------
SEGMENTNO              :       ID of the segment, starts at 0
SEGMENTDURATION        :       Duration (in frames) of the segment
COLOUR1		       :       COLOUR=6 means segment is kept, 2 means deleted.
COLOUR2		       :       Field used for additional info

X_OR_NORMALISED	       :       Normalised x-orientation
Y_OR_NORMALISED	       :       Normalised y-orientation
--------------------------------------------------------------------------------
