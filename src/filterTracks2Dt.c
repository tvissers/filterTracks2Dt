/*
    This file is part of filterTracks2Dt.

    filterTracks2Dt is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    filterTracks2Dt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with filterTracks2Dt.  If not, see <http://www.gnu.org/licenses/>.
*/

/* About this program

filterTracks2Dt is designed to read a file containing trajectories and removing the parts where trajectories are crossing, producing new trajectories that do not cross. Written by Teun Vissers. 

Please cite [T. Vissers et al., Science Advances, 4, 4, eaao1170, 2018] if you use it for a scientific publication.
Published under the GPLv3 license.

*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <glob.h>
#include "namelist.h"

/* Variables: Global variables indicated with CAPITAL LETTERS
  TT               - Measure for distance at which two trajectories are considered to be crossing
  OUTPUTDIR[1024]  - Output directory
  DEBUG            - Sets the amount of checks and DEBUG output that is generated.
                    0 is minimal (no output), higher numbers will generate more output.
  TRACKINPUTFILE   - Input file containing the original trajectories
  N_TRACKS	   - Number of trajectories       
  N_FRAMES	   - Number of frames
  CELLLIST	   - Cell-list to speed up the calculations.. 
*/

float TT=5.0;
char TRACKINPUTFILE[1000]="";
char OUTPUTDIR[1000]="";
int DEBUG=0;
int N_TRACKS;
int N_FRAMES;
int **** CELLLIST;

NameList nameList[] = {
	NameR (TT),		// Measure for distance at which two trajectories are considered to be crossing
	NameC (OUTPUTDIR),      // Output directory
	NameC (TRACKINPUTFILE),	// Trajectories input file 
	NameI (DEBUG),    	// More verbose output
};

#include "namelist.c"

typedef struct {
  int x;
  int y;
} tVectori;

tVector** trackTable;
int** trackDetails;

double rc_frameWidth=1040.0;
double rc_frameHeight=1040.0;

double rc_y=10; // r_c = critical box_size for consideration, fast particles need large r_c else they are not detected
double rc_x=10;

double rc_inv_y;
double rc_inv_x;

int rc_maximumNumberofBoxes_x;
int rc_maximumNumberofBoxes_y;

int m_jumps=0;
int m_crossings=0;
int m_new=0;

int frame; //frame number for timeseries

void mygetline(char* str,FILE *f)
{
  	int comment=1;
  	while(comment)
  	{
		if (!fgets(str,256,f)) return;
		if(str[0]!='#') comment=0;
	}
}

/* Function: dot
        Calculate dot product between a and b

        Parameters:
                a - vector
                b - vector

        Returns:
                dot product between a and b
*/
inline double dot(dir a, dir b)
{
  	return a.x*b.x+a.y*b.y;
}

/* Function: cross
        Calculate cross product between M and N

        Parameters:
                M - vector
                N - vector

        Returns:
                cross product between M and N
*/
double cross(dir M, dir N)
{
        double z;
        z = M.x*N.y-M.y*N.x;
        return z;
}

/* Function: normalizeVector
        Normalises a vector with x and y coordinate

        Parameters:
                v - vector to normalise

        Returns:
                v - normalised vector
*/
dir normalizeVector(dir v)

{
        double norm = sqrt(v.x*v.x+v.y*v.y);
	if (norm > 0)
	{
        	v.x = v.x/norm;
	        v.y = v.y/norm;
	}
	else
	{
		v.x = 0.0;
		v.y = 0.0;
	}
        return v;
}

/* Function: allocateArrays
        Allocates memory to store arrays
*/
void allocateArrays()

{
        trackTable= (tVector**) malloc(sizeof(tVector*)*N_TRACKS);
	trackDetails=(int**) malloc(sizeof(int*)*N_TRACKS);
}

/* Function: readTracks
        Reads trajectories from input file

	Returns: 
		version - file version
*/
double readTracks()
{
	printf("Loading tracks.. \n");
	FILE* fpt=fopen(TRACKINPUTFILE, "r");
	char str[256];
	mygetline(str,fpt);
	float version = 0.0;

	sscanf(str, "&%i &%i %e", &N_TRACKS, &N_FRAMES, &version);

	if (version == 0.0)
        {
                printf("Error, undefined file format version!\n");
                exit(666);
        }

	int tmp;
	printf("Found %i tracks in %i frames\n", N_TRACKS, N_FRAMES);
	printf("File format version: %f\n", version);

	allocateArrays();

	float x;
	float y;
	float q;
	float r;
	float s;
	float ar1;
	float ar2;
	float ar3;
	float width;
	float accuracy;
	int thispart;

	int trackLength=0;

	int i = 0;
	for (i =0; i < N_TRACKS; i++)
	{
		mygetline(str,fpt);
		sscanf(str,"&%i &%i", &tmp, &trackLength);

		trackDetails[i]= (int*) malloc(sizeof(int)*4);
		trackDetails[i][0] = trackLength;

		trackTable[i]= (tVector*) malloc(sizeof(tVector)*trackLength);

		int frameNo=0;
		int k;  	
		for (k =0; k < trackLength; k++)
		{
			mygetline(str,fpt);

			if (version > 0)
				sscanf(str,"%i %i %e %e %e %e %e %e %e %e", &frameNo, &thispart, &x, &y, &q, &r, &s, &ar1, &ar2, &ar3);	
			else if (version < 0)
				sscanf(str,"%i %i %e %e %e %e %e %e %e %e %e %e", &frameNo, &thispart, &x, &y, &q, &r, &s, &ar1, &ar2, &ar3, &width, &accuracy);	
					
			if (k ==0)
			{
				trackDetails[i][1] = frameNo;
				trackDetails[i][2] = 0; // number of New VALID tracks
				trackDetails[i][3] = 0; // number of CHOPPED tracks
			}
			
			trackTable[i][k].x= (double)x;
			trackTable[i][k].y= (double)y;
			trackTable[i][k].orientation.x= (double)q; // this should be a vector pointing from COM to center of spherocylinder cap.
			trackTable[i][k].orientation.y= (double)r;

			if (trackTable[i][k].x > rc_frameWidth || trackTable[i][k].y > rc_frameHeight || trackTable[i][k].x <= 0 || trackTable[i][k].y <= 0)
			{
				printf("Coordinate out of bounds: %f %f\n", trackTable[i][k].x, trackTable[i][k].y);
				exit(666);
			}

			dir normed;

			normed.x = trackTable[i][k].orientation.x;
			normed.y = trackTable[i][k].orientation.y;
			normed = normalizeVector(normed);

			trackTable[i][k].normalized_orientation.x = normed.x;		
			trackTable[i][k].normalized_orientation.y = normed.y;		

			trackTable[i][k].pol1.x = 0.0;
			trackTable[i][k].pol1.y = 0.0;
			trackTable[i][k].pol2.x = 0.0;
			trackTable[i][k].pol2.y = 0.0;

			trackTable[i][k].length=(double)s;	// this should be the optical length, from end to end
			trackTable[i][k].width=(double)width;	// this should be the optical length, from end to end
			trackTable[i][k].accuracy=(double)accuracy;	// this should be the accuracy of the fit, low values mean width-axis and length-axis cannot be distinguished
			trackTable[i][k].ar1=(double)ar1;
			trackTable[i][k].ar2=(double)ar2;
			trackTable[i][k].ar3=(double)ar3;
			trackTable[i][k].n=thispart;
			
			trackTable[i][k].x_cell = 0;
			trackTable[i][k].y_cell = 0;
			trackTable[i][k].neighbours = 0;

			trackTable[i][k].state = 0;
			trackTable[i][k].jump = 0;
			
			trackTable[i][k].bodyOrientation_cor = -10.0;
			trackTable[i][k].swimmingDirection_cor = -10.0;
			trackTable[i][k].swimmingSpeed = -10.0;
			trackTable[i][k].swimmingInclination = -10.0;
		}
	}	

	fclose(fpt);
	printf("Done..\n");
	return version;
}

/* Function: closeFiles
        Closes some files
*/
void closeFiles()

{
  	printf("Closing..\n");
	char buffer[1024];
	snprintf(buffer, sizeof(buffer), "%sdetails.info", OUTPUTDIR); 
	FILE* infoFile = fopen(buffer, "w");

   	fprintf(infoFile, "This dataset contains trajectories of rod-shaped particles.\n\n");
	
	fprintf(infoFile, "Distance at which two tracks are considered crossing: %f\n", TT);
	fprintf(infoFile, "Travelled distance between two frames that constitutes a jump: %f\n", 2*TT);
	fprintf(infoFile, "Found %i original tracks.\n", N_TRACKS);
   	fprintf(infoFile, "Found %i crossings.\n", m_crossings);
   	fprintf(infoFile, "Found %i jumps.\n", m_jumps);
   	fprintf(infoFile, "Output contains %i tracks.\n", m_new);
   	
	fclose(infoFile);
   	printf("Done..\n");
}

/* Function: freeCells
	frees up cell-list from memory
*/
void freeCells()
{
   	printf("Freeing cells.\n");

	int x;
	int y;
	int z;

	for (z = 0; z < N_FRAMES; z++)
	{
		for (x = 0; x < rc_maximumNumberofBoxes_x; x++)
		{
			for (y = 0; y < rc_maximumNumberofBoxes_y; y++)
			{
				free(CELLLIST[z][x][y]);
			}
			free(CELLLIST[z][x]);
		}
		free(CELLLIST[z]);
	}
	free(CELLLIST);

	printf("Done..\n");
}

/* Function: writeNewTrack
	Appends a new trajectory (without fragments where there is overlap) to file

        Parameters:
                file - output file for new trajectories
                i - trajectory number
                t - trajectory number for this new trajectory
                sublength - length of this fragment
                firstFrame - frame at which the original input trajectory starts
                start - frame inside original trajectory to start
                stop - frame inside original trajectory to end
*/
void writeNewTrack(FILE* file, int i, int t, int subLength, int firstFrame, int start, int stop)
{
	int k= 0;
        fprintf(file, "&%i &%i\n", t, subLength);
	for (k = start; k < stop; k++)
	{
		fprintf(file, "%i %i %f %f %f %f %f %f %f %f %f %f\n", firstFrame+k, trackTable[i][k].n, trackTable[i][k].x, trackTable[i][k].y, trackTable[i][k].orientation.x, trackTable[i][k].orientation.y, trackTable[i][k].length, trackTable[i][k].ar1, trackTable[i][k].ar2, trackTable[i][k].ar3, trackTable[i][k].width, trackTable[i][k].accuracy);
	}

}

/* Function: writeCheckTrack
        Appends trajectory fragments to output file, to allow for visual inspection

        Parameters:
                file - output file for check trajectory fragments
                i - trajectory number
                t - trajectory number for this trajectory fragment
                sublength - length of this fragment
                firstFrame - frame at which the original input trajectory starts
                start - frame inside original trajectory to start
                stop - frame inside original trajectory to end
                CHECK - 0 for fragment that is chopped away, 1 for fragment that is preserved
*/
void writeCheckTrack(FILE* file, int i, int t, int subLength, int firstFrame, int start, int stop, int CHECK)
{
        int k= 0;
	int COLOR1 = 0;
	float COLOR2 = 0;

	if (CHECK == 0)	// this part of the track disappears!
	{
		COLOR1 = 2;
		COLOR2 = 1000.0;
	}
	if (CHECK == 1) // this part of the track is kept!
	{
		COLOR1 = 6;
		COLOR2 = 0.00001;
	}
	
        fprintf(file, "&%i &%i &%i &%f\n", t, subLength, COLOR1, COLOR2);
        for (k = start; k < stop; k++)
        {
                fprintf(file, "%i %i %f %f %f %f %f %f %f %f\n", firstFrame+k, trackTable[i][k].n, trackTable[i][k].x, trackTable[i][k].y, trackTable[i][k].normalized_orientation.x, trackTable[i][k].normalized_orientation.y, trackTable[i][k].length, 0.5, 0.5, 0.0);

        }
}

/* Function: writeFixedTrack
	Organises to appends a fixed track to output file, an to append chopped and preserved parts to a check file (to allow for visual inspection)

        Parameters:
                file - to write new trajectories
                file_check - to write check trajectories
                i - trajectory number
		t - trajectory number

        Returns:
		number of frames in new track
*/
int writeFixedTrack(FILE* file, FILE* file_check, int i, int *t)

{	
	int start=0;
	int stop=0;

	int subLength = 0;
	int pause = 0;
	int check = 0;

	int t_check = 0;
	int m = 0;
	int trackLength = trackDetails[i][0];
	int firstFrame = trackDetails[i][1];
	
	int start_check=0;
	int stop_check=0;

        for(m=0; m < trackLength; m++) // start going through track i
        {
		if (trackTable[i][m].state > 0 && pause == 0) // when arrived at a chopped part
		{
			pause = 1;
			stop = m;
			subLength = stop-start;
			if (subLength > 0)
			{
				writeNewTrack(file, i, *(t), subLength, firstFrame, start, stop); 
				writeCheckTrack(file_check, i, t_check, subLength, firstFrame, start, stop, 1); 
				*(t) = *(t) + 1;
				t_check++;
				check++;
				//if (i < 5)
				//	printf("found: %i\n", start);
			}
			start_check = stop;
		}
		if (trackTable[i][m].state == 0 && pause == 1)
		{
			pause = 0;
			stop_check = m;
			int subLength_check = stop_check-start_check;
                        if (subLength > 0)
                        {
                                writeCheckTrack(file_check, i, t_check, subLength_check, firstFrame, start_check, stop_check, 0);
				t_check++;
                        }

			start = m;
		}
		
        }

	// write last part of the track:
	if (pause == 0)
	{
		stop = trackLength;
		subLength = stop-start;
		if (subLength > 0)
		{
			writeNewTrack(file, i, *(t), subLength, firstFrame, start, stop);
			writeCheckTrack(file_check, i, t_check, subLength, firstFrame, start, stop, 1); 
                        *(t) = *(t) + 1;
			t_check++;
                        check++;
			//if (i < 5)
			//	printf("found: [i]: %i-%i\n", start, stop);
                }
	}
	return check;
}

/* Function: writeFixedTracks
	write fixed tracks to an outputfile

	Parameters:
		version - file format version	
*/
void writeFixedTracks(float version)
{
	printf("Writing fixed tracks to new file..\n");
        
	char buffer[1024];
	char buffer2[1024];
        FILE* file;
        FILE* file_check;

	int total = 0;
	int total_check = 0;
        int t = 0;

	int i = 0;
	for (i = 0; i < N_TRACKS; i++)
        {
		total += trackDetails[i][2];
		total_check += trackDetails[i][3];
 	}
	m_new = total;

        snprintf(buffer, sizeof(buffer), "%stracks_fixed.dat", OUTPUTDIR);
        file = fopen(buffer, "w");
        fprintf(file, "&%i &%i %f\n", total, N_FRAMES, version);
        
	snprintf(buffer2, sizeof(buffer2), "%stracks_fixed.chk", OUTPUTDIR);
        file_check = fopen(buffer2, "w");
        fprintf(file_check, "&%i &%i %f\n", total_check, N_FRAMES, 1.0);

	int processed = 0;
	for (i = 0; i < N_TRACKS; i++)
		processed += writeFixedTrack(file, file_check, i, &t);

        fclose(file);
        fclose(file_check);
	printf("Originally there where %i tracks. \n", N_TRACKS);
	printf("Now there are %i (should be %i) \n", processed, total);
	printf("Last track: %i \n", t);
	printf("Done. \n");
}

/* Function: findNeighbours
	Finds crossings in trajectories, and marks parts to cut it to remove overlapping parts. This function uses cell-lists.
*/
void findNeighbours()
{
	// note three indexes:
	// j : real frame number (image)
	// k : index in the track we are iterating
	// h : index in a track we are comparing with

	int overlaps=0;
	int points=0;
	int i = 0;
	int j = 0;

	for (i = 0; i < N_TRACKS; i++)
        {
		int trackLength = trackDetails[i][0];
                int firstFrame = trackDetails[i][1];
	
		int state = 0;	
		int subLength = 0;
		int k = 0;
                for (k = 0; k < trackLength; k++)
                {
			points++;
			j = k + firstFrame;

			int j_box_x=0;
                        int j_box_y=0;

			int begin_x = trackTable[i][k].x_cell;
			if (trackTable[i][k].x_cell > 0)
				begin_x = trackTable[i][k].x_cell-1;
	
			int until_x = trackTable[i][k].x_cell+1;
			if (trackTable[i][k].x_cell < (rc_maximumNumberofBoxes_x-1) )
				until_x = trackTable[i][k].x_cell+2;
			
			int begin_y = trackTable[i][k].y_cell;
			if (trackTable[i][k].y_cell > 0)
				begin_y = trackTable[i][k].y_cell-1;
	
			int until_y = trackTable[i][k].y_cell+1;
			if (trackTable[i][k].y_cell < (rc_maximumNumberofBoxes_y-1) )
				until_y = trackTable[i][k].y_cell+2;
				
			int foundPositive = 0;

			double r_self=0.0;
			if (k > 0)
			{
				double dX = (trackTable[i][k].x-trackTable[i][k-1].x);
				double dY = (trackTable[i][k].y-trackTable[i][k-1].y);
				r_self = dX*dX + dY*dY;
				if (r_self > 2.0*TT*2.0*TT) // huge jump inside this track
				{
					foundPositive += 1;
					trackTable[i][k].state = 1; // something is wonky about this track.
					trackTable[i][k].jump = 1; // something is wonky about this track.
				}
			}

			for (j_box_x = begin_x; j_box_x < until_x; j_box_x++)
                        {       // surrounding cells in x-direction 
				for (j_box_y = begin_y; j_box_y < until_y; j_box_y++)
				{// surrounding cells in y-direction:
					if (CELLLIST[j][j_box_x][j_box_y] != NULL)
                                        {
						int n = 0;
                                                for (n = 1; n < (CELLLIST[j][j_box_x][j_box_y][0]+1); n++)
                                                {
                                                        int i_n = CELLLIST[j][j_box_x][j_box_y][n];								
						
							if (i != i_n)	
							{
								int h = j-trackDetails[i_n][1]; // index of particle in track i_n
				
								double dx = (trackTable[i][k].x-trackTable[i_n][h].x);
								double dy = (trackTable[i][k].y-trackTable[i_n][h].y);

								double r2 = dx*dx + dy*dy;
								if ( r2 < (TT*TT) ) // two tracks are close together
								{
									overlaps++;
									foundPositive = 1;
									trackTable[i][k].neighbours++; // found a neighbour
									trackTable[i][k].state = 1; // something is wonky about this track.
								}
							}
                                                }
                                        }
					
				}
			}

			if (foundPositive > 0) // this track crosses another one or passes by too closely.
			{
				if (state == 0)
				{
					if (trackTable[i][k].jump == 1)
					{
						//printf("jump of %f pixels! (neighbours: %i)\n", sqrt(r_self), trackTable[i][k].neighbours);
						m_jumps++;
					}
					if (trackTable[i][k].neighbours > 0)
					{
						m_crossings++;
					}
					trackDetails[i][3]++; // Chop chop
					
					state = 1;
					subLength = 0;
				}
			}
			else
			{
				state = 0;
				if (subLength == 0)
				{
					trackDetails[i][2]++; // beginning of a subtrack, so should add one
					trackDetails[i][3]++; // Chop chop
				}
				subLength++;
			}
		}
	}

	printf("Done. Found %i overlaps in %i points (%i tracks in total)\n", overlaps, points, N_TRACKS);
}

/* Function: setupCells
        Sets up the cells for the cell-list
*/
void setupCells()
{
	printf("Attributing cells to particles in %i tracks.. \n", N_TRACKS);
        printf("Number of frames: %i\n", N_FRAMES);

	int i = 0; // track
	int j = 0; // frame number
	int k = 0;

        for (i = 0; i < N_TRACKS; i++)
        {
		int trackLength = trackDetails[i][0];
	        int firstFrame = trackDetails[i][1];
                
		for (k = 0; k < trackLength; k++)
                {
			j = k + firstFrame; // the real frame number (image), instead of the index inside the track.
                        int x_cell = ( (int)ceil(trackTable[i][k].x * rc_inv_x) )-1;
                        int y_cell = ( (int)ceil(trackTable[i][k].y * rc_inv_y) )-1;                       
	
                        if (x_cell < 0 || y_cell < 0)
                        {
                                printf("Look for errors in input file!\n");
                                printf("Cells: %i %i, Particle coordinates: %f %f\n", x_cell, y_cell, trackTable[i][j].x, trackTable[i][j].y);
                                printf("Track %i, Frame %i \n", i, j);
                                exit(666); // should be impossible!
                        }
                        if (CELLLIST[j][x_cell][y_cell] == NULL)                                        // major bug fixed 09-07-2008
                        {
                                CELLLIST[j][x_cell][y_cell] = (int*) malloc(1*sizeof(int));
                                CELLLIST[j][x_cell][y_cell][0] = 0;
                        }
                        CELLLIST[j][x_cell][y_cell][0]++;
                        CELLLIST[j][x_cell][y_cell] = (int*)realloc(CELLLIST[j][x_cell][y_cell], (CELLLIST[j][x_cell][y_cell][0]+1)*sizeof(int));

                        if (CELLLIST[j][x_cell][y_cell] == NULL)
                        {
                                printf("Memory full!\n");
                                exit(666);
                        }

                        CELLLIST[j][x_cell][y_cell][CELLLIST[j][x_cell][y_cell][0]] = i; // i denotes the track in this cell in frame j
                        
			trackTable[i][k].x_cell = x_cell;        // labelling the particle in track i with CELLLIST info
                        trackTable[i][k].y_cell = y_cell;        // labelling the particle in track i with CELLLIST info
                }
        }
        printf("Done.. \n");
}

/* Function: createCellList
        Creates the cell-lists
*/
void createCellList()
{
 	printf("Creating cell list..\n");

        rc_inv_y = 1.0/rc_y;
        rc_inv_x = 1.0/rc_x;

        printf("Creating 2D cells with dimensions %f x %f.. \n", rc_x, rc_y);

        CELLLIST = (int****) malloc((N_FRAMES)*sizeof(int***));   // array of 2D CELLLISTs

        rc_maximumNumberofBoxes_x = (int)ceil((double)rc_frameWidth/rc_x);   
        rc_maximumNumberofBoxes_y = (int)ceil((double)rc_frameHeight/rc_y);

        printf("Allocating memory.. \n");

        int x;
        int y;
        int z;

        for (z = 0; z < N_FRAMES; z++)
        {
                CELLLIST[z] = (int***) malloc(rc_maximumNumberofBoxes_x*sizeof(int**));
                for (x = 0; x < rc_maximumNumberofBoxes_x; x++)
                {
                        CELLLIST[z][x] = (int**) malloc(rc_maximumNumberofBoxes_y*sizeof(int*));
                        for (y = 0; y < rc_maximumNumberofBoxes_y; y++)
                        {
                                CELLLIST[z][x][y] = NULL;
                        }
                }
        }
 	printf("Done..\n");	
}

int main(int argc, char *argv[])
{
   	GetNameList(argc,argv);
	PrintNameList (stdout);

	printf("TT: %f\n", TT);
   	float version = readTracks();
   	createCellList();
   	setupCells();
   	findNeighbours();
	writeFixedTracks(version);
	freeCells();
	closeFiles();

   	return 0;
}
